# Your Very First Yoga Class - What to Expect

So you've finally made the huge leap into the unknown - your very first yoga class is approaching. You've picked which class to go to, learned where to park and how much the class will cost, however, what should you anticipate when you walk through the doors? It is ideal to attend a few yoga classes to get a basic understanding of yoga before you attend a [yoga retreat in Nepal](https://nepalyogateachertraining.com/one-month-yoga-retreat-in-nepal/). 

Yoga - more than just an exercise class

The first yoga class is a time of discovery. You'll find that not every yoga professional is an extremely committed, eco-conscious vegetarian; that designer equipment will not assist you in a challenging pose any faster, and you will discover secrets about your body and your mind that even you never ever knew. Sound interesting? Well, that's yoga for you! It's more than simply an exercise class.

Yoga teachers have got your back - there's no requirement to stress

Yoga people tend to be a friendly bunch. The dynamics of each yoga class and each studio vary significantly however all ought to be welcoming and inclusive, not judgemental. Everyone in the class is an equivalent in the eyes of the instructor so stop worrying about your lack of knowledge or failure to touch your toes, it simply does not matter.

Yoga classes are non-competitive

Don't try to stay up to date with other people in the class. Yoga is a specific practice and everybody is different. Even yoga teachers aren't best at every posture. All of us have our physical limitations and part of yoga is learning to appreciate your body, be kind to it, and do not push it further than it wants to go. Your body will open into positions when it is all set, so be patient throughout the early phases of your practice.

, strong > What to require to yoga classes

On a practical note, what do you take to a class? Water is an excellent idea unless you are practicing Ashtanga when water needs to not be consumed during the class. You might wish to take a small towel and, if health concerns you, your own mat, and an eye pillow for the relaxation at the end.

Yoga mats

Studios normally supply mats so it is not important to purchase your own; this choice boils down to personal choice. Some studios might not sanitize and upgrade their mats as routinely as possibly they must and practicing on a stale smelling mat is not the most enjoyable experience. On an ecological note, if this is your first ever experience of yoga it is probably best not to purchase your own mat till you understand that the practice is for you.

Studio rules

Stinky mats bring us on to some rather standard studio rules which are all frequently overlooked. If you utilize one of the studio's mats, you should clean it after the class. This only takes a minute and you will discover a disinfectant spray and cloths near where the mats are kept. Please take a moment to do this; if a single person cleans their mat then others tend to follow.

Yoga presents first up

The content of your class will differ depending on the design your chosen studio follows. Typically speaking though, newbies' classes focus on the asanas or postures and tend not to include any sophisticated breathing methods or shouting.